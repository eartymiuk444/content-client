package info.earty.content.presentation;

import info.earty.content.presentation.data.DraftCardJsonDto;
import info.earty.content.presentation.data.PublishedCardJsonDto;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Tag(name = "WorkingCardQuery")
@Path("/content/workingCard/query/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface WorkingCardQueryApi {

    @GET
    @Path("/draft")
    DraftCardJsonDto getDraftCard(@QueryParam("workingCardId") String workingCardId);
    @GET
    @Path("/published")
    PublishedCardJsonDto getPublishedCard(@QueryParam("workingCardId") String workingCardId);

    @GET
    @Path("/draftCards")
    Set<DraftCardJsonDto> getDraftCardsByWorkingPageId(@QueryParam("workingPageId") String workingPageId);
    @GET
    @Path("/publishedCards")
    Set<PublishedCardJsonDto> getPublishedCardsByWorkingPageId(@QueryParam("workingPageId") String workingPageId);

}
