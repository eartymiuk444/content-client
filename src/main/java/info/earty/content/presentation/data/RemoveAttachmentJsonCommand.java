package info.earty.content.presentation.data;

import lombok.Data;

@Data
public class RemoveAttachmentJsonCommand {

    private String workingCardId;
    private String attachmentId;

}
