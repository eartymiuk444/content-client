package info.earty.content.presentation.data;

import lombok.Data;

import java.util.List;
import java.util.Map;
@Data
public class PublishedPageJsonDto {

    private String title;
    private Map<String, OutlineItemDto> allOutlineItems;

    @Data
    public static class OutlineItemDto {
        private String workingCardId;
        private String title;
        private String fragment;
        private List<OutlineSubItemDto> subItems;
    }

    @Data
    public static class OutlineSubItemDto {
        private String workingCardId;
        private String title;
        private String fragment;
    }
}
