package info.earty.content.presentation.data;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class SiteMenuJsonDto {

    private String siteMenuId;
    private Integer rootDirectoryId;
    private Map<Integer, DirectoryDto> allDirectories;

    @Data
    public static class DirectoryDto {
        private Integer directoryId;
        private String name;
        private String pathSegment;
        private List<DirectoryItemDto> items;
    }

    @JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS, property = "class")
    public interface DirectoryItemDto {
        String getName();
        String getPathSegment();
    }

    @Data
    public static class SubDirectoryDto implements DirectoryItemDto {
        private Integer directoryId;
        private String name;
        private String pathSegment;
    }

    @Data
    public static class PageDto implements DirectoryItemDto {
        private String workingPageId;
        private String name;
        private String pathSegment;
    }

}
