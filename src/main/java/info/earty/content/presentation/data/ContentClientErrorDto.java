package info.earty.content.presentation.data;

import lombok.Data;

@Data
public class ContentClientErrorDto {
    private String message;
}
