package info.earty.content.presentation.data;

import lombok.Data;

@Data
public class RemoveImageJsonCommand {

    private String workingCardId;
    private String imageId;

}
