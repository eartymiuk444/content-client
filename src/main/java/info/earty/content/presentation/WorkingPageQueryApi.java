package info.earty.content.presentation;

import info.earty.content.presentation.data.DraftJsonDto;
import info.earty.content.presentation.data.PublishedPageJsonDto;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Tag(name = "WorkingPageQuery")
@Path("/content/workingPage/query/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface WorkingPageQueryApi {
    @GET
    @Path("/draft")
    DraftJsonDto getDraft(@QueryParam("workingPageId") String workingPageId);

    @GET
    @Path("/published")
    PublishedPageJsonDto getPublishedPage(@QueryParam("workingPageId") String workingPageId);

}
