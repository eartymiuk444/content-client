package info.earty.content.presentation;

import info.earty.content.presentation.command.menu.*;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Tag(name = "SiteMenuCommand")
@Path("/content/siteMenu/command/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface SiteMenuCommandApi {
    @POST
    @Path("/create")
    void create();

    @POST
    @Path("/addDirectory")
    void addDirectory(AddDirectoryJsonCommand jsonCommand);

    @POST
    @Path("/moveDirectoryUp")
    void moveDirectoryUp(MoveDirectoryUpJsonCommand jsonCommand);

    @POST
    @Path("/moveDirectoryDown")
    void moveDirectoryDown(MoveDirectoryDownJsonCommand jsonCommand);

    @POST
    @Path("/moveDirectory")
    void moveDirectory(MoveDirectoryJsonCommand jsonCommand);

    @POST
    @Path("/addPage")
    void addPage(AddPageJsonCommand jsonCommand);

    @POST
    @Path("/movePageUp")
    void movePageUp(MovePageUpJsonCommand jsonCommand);

    @POST
    @Path("/movePageDown")
    void movePageDown(MovePageDownJsonCommand jsonCommand);

    @POST
    @Path("/movePage")
    void movePage(MovePageJsonCommand jsonCommand);

    @DELETE
    @Path("/removeDirectory")
    void removeDirectory(RemoveDirectoryJsonCommand jsonCommand);

    @DELETE
    @Path("/removePage")
    void removePage(RemovePageJsonCommand jsonCommand);

    @POST
    @Path("/editDirectory")
    void editDirectory(EditDirectoryJsonCommand jsonCommand);

    @POST
    @Path("/changePagePathSegment")
    void changePagePathSegment(ChangePagePathSegmentJsonCommand jsonCommand);
}
