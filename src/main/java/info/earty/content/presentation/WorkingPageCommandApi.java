package info.earty.content.presentation;

import info.earty.content.presentation.command.page.*;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Tag(name = "WorkingPageCommand")
@Path("/content/workingPage/command/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface WorkingPageCommandApi {
    @POST
    @Path("/addOutlineItem")
    void addOutlineItem(AddOutlineItemJsonCommand jsonCommand);

    @POST
    @Path("/changeOutlineItemFragment")
    void changeOutlineItemFragment(ChangeOutlineItemFragmentJsonCommand jsonCommand);

    @DELETE
    @Path("/removeOutlineItem")
    void removeOutlineItem(RemoveOutlineItemJsonCommand jsonCommand);

    @POST
    @Path("/moveOutlineItem")
    void moveOutlineItem(MoveOutlineItemJsonCommand jsonCommand);

    @POST
    @Path("/moveOutlineSubItemUp")
    void moveOutlineSubItemUp(MoveOutlineSubItemUpJsonCommand jsonCommand);

    @POST
    @Path("/moveOutlineSubItemDown")
    void moveOutlineSubItemDown(MoveOutlineSubItemDownJsonCommand jsonCommand);

    @POST
    @Path("/discardDraft")
    void discardDraft(DiscardDraftJsonCommand jsonCommand);

    @POST
    @Path("/publish")
    void publish(PublishJsonCommand jsonCommand);

    @POST
    @Path("/changeTitle")
    void changeTitle(ChangeTitleJsonCommand jsonCommand);

    @POST
    @Path("/changeOutlineItemTitle")
    void changeOutlineItemTitle(ChangeOutlineItemTitleJsonCommand jsonCommand);
}
