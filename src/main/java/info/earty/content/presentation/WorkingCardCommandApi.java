package info.earty.content.presentation;

import info.earty.content.presentation.command.card.ChangeTextJsonCommand;
import info.earty.content.presentation.data.RemoveAttachmentJsonCommand;
import info.earty.content.presentation.data.RemoveImageJsonCommand;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Tag(name = "WorkingCardCommand")
@Path("/content/workingCard/command/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface WorkingCardCommandApi {

    @POST
    @Path("/changeText")
    void changeText(ChangeTextJsonCommand jsonCommand);

    @DELETE
    @Path("/removeImage")
    void removeImage(RemoveImageJsonCommand jsonCommand);

    @DELETE
    @Path("/removeAttachment")
    void removeAttachment(RemoveAttachmentJsonCommand jsonCommand);

}
