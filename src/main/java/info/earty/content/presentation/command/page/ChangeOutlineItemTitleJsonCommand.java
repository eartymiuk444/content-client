package info.earty.content.presentation.command.page;

import lombok.Data;

@Data
public class ChangeOutlineItemTitleJsonCommand {

    private String workingPageId;
    private String workingCardId;
    private String title;

}
