package info.earty.content.presentation.command.page;

import lombok.Data;

@Data
public class PublishJsonCommand {
    private String workingPageId;
}
