package info.earty.content.presentation.command.page;

import lombok.Data;

@Data
public class ChangeTitleJsonCommand {
    private String workingPageId;
    private String title;
}
