package info.earty.content.presentation.command.menu;

import lombok.Data;

@Data
public class EditDirectoryJsonCommand {
    private String siteMenuId;
    private Integer directoryId;
    private String name;
    private String pathSegment;
}
