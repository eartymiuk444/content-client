package info.earty.content.presentation.command.page;

import lombok.Data;

@Data
public class DiscardDraftJsonCommand {
    private String workingPageId;
}
