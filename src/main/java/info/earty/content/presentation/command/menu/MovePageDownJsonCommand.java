package info.earty.content.presentation.command.menu;

import lombok.Data;

@Data
public class MovePageDownJsonCommand {

    private String siteMenuId;
    private String workingPageId;
    private Integer parentDirectoryId;

}
