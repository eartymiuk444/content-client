package info.earty.content.presentation.command.page;

import lombok.Data;

@Data
public class MoveOutlineItemJsonCommand {
    private String workingPageId;
    private String workingCardId;
    private Boolean moveToRoot;
    private String parentWorkingCardId;
}
