package info.earty.content.presentation.command.page;

import lombok.Data;

@Data
public class RemoveOutlineItemJsonCommand {

    private String workingPageId;
    private String workingCardId;

}
