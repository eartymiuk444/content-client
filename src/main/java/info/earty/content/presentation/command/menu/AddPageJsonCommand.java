package info.earty.content.presentation.command.menu;

import lombok.Data;

@Data
public class AddPageJsonCommand {
    private String siteMenuId;
    private String title;
    private Integer parentDirectoryId;
    private String pathSegment;
}
