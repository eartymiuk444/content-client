package info.earty.content.presentation.command.card;

import lombok.Data;

@Data
public class ChangeTextJsonCommand {
    private String workingCardId;
    private String text;
}
