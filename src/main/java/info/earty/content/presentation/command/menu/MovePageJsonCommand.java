package info.earty.content.presentation.command.menu;

import lombok.Data;

@Data
public class MovePageJsonCommand {

    private String siteMenuId;
    private String workingPageId;
    private Integer parentDirectoryId;

}
