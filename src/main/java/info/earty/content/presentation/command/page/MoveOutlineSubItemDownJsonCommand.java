package info.earty.content.presentation.command.page;

import lombok.Data;

@Data
public class MoveOutlineSubItemDownJsonCommand {
    private String workingPageId;
    private String parentWorkingCardId;
    private String workingCardId;
}
