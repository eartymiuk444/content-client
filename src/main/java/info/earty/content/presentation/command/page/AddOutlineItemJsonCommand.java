package info.earty.content.presentation.command.page;

import lombok.Data;

@Data
public class AddOutlineItemJsonCommand {
    private String workingPageId;
}
