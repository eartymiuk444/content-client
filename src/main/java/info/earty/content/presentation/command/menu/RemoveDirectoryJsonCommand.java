package info.earty.content.presentation.command.menu;

import lombok.Data;

@Data
public class RemoveDirectoryJsonCommand {

    private String siteMenuId;
    private Integer directoryId;

}
