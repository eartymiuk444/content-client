package info.earty.content.presentation.command.menu;

import lombok.Data;

@Data
public class AddDirectoryJsonCommand {
    private String siteMenuId;
    private String name;
    private Integer parentDirectoryId;
    private String pathSegment;
}
