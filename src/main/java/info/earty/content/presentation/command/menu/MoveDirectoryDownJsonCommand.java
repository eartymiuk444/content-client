package info.earty.content.presentation.command.menu;

import lombok.Data;

@Data
public class MoveDirectoryDownJsonCommand {

    private String siteMenuId;
    private Integer directoryId;
    private Integer parentDirectoryId;

}
