package info.earty.content.presentation.command.menu;

import lombok.Data;

@Data
public class RemovePageJsonCommand {

    private String siteMenuId;
    private String workingPageId;

}
