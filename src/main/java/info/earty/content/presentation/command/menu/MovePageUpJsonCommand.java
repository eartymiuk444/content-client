package info.earty.content.presentation.command.menu;

import lombok.Data;

@Data
public class MovePageUpJsonCommand {

    private String siteMenuId;
    private String workingPageId;
    private Integer parentDirectoryId;

}
