package info.earty.content.presentation.command.page;

import lombok.Data;

@Data
public class ChangeOutlineItemFragmentJsonCommand {
    private String workingPageId;
    private String workingCardId;
    private String fragment;
}
