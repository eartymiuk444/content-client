package info.earty.content.presentation.command.menu;

import lombok.Data;

@Data
public class ChangePagePathSegmentJsonCommand {
    private String siteMenuId;
    private String workingPageId;
    private String pathSegment;
}
