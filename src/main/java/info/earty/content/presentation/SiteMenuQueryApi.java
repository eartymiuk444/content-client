package info.earty.content.presentation;

import info.earty.content.presentation.data.SiteMenuJsonDto;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Tag(name = "SiteMenuQuery")
@Path("/content/siteMenu/query/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface SiteMenuQueryApi {

    @GET
    SiteMenuJsonDto get();
}
